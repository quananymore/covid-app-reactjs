import React from 'react'
import Patient from './components/PatientComponent';
import PatientList from './components/PatientList';

import { 
  BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Patient2 from './components/Patient';
import ModalAddSchedule from './layouts/ModuleAddSchedule';
import ModalAddContact from './layouts/ModalAddContact';
import AddContact from './layouts/AddContact';
function App(props) {
  return (
    <Router>
        <div className="App">
        <Switch>
          <Route exact path="/" component={PatientList} />
          <Route exact path="/patient/:id" component={Patient2} />
           <Route exact path="/contact" component={AddContact} />
          {/*<Route exact path="/users/add" component={AddUser} />
          <Route exact path="/users/edit/:id" component={EditUser} />
          <Route exact path="/users/:id" component={User} />
          <Route component={NotFound} /> */}
        </Switch>
        </div>
    </Router>
  );
}

export default App;


