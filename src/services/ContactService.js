import http from "../commons/http-common";
class ContactService{
    getContact(id){
        return http.get(`/contact/${id}`);
    }

    create(data){
        return http.post("/contact/add",data);
    }
  
}

export default new ContactService();