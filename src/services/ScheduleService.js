import http from "../commons/http-common";
class ScheduleService{

    getSchedule(id){
        return http.get(`/schedule/${id}`);
    }

    create(data){
        return http.post("/schedule/add",data);
    }

    update(data){
        return http.post("/patient/update",data);
    }

    delete(id){
        return http.delete(`/patient/delete/${id}`)
    }

    search(name,status){
        return http.get(`/patients?name=${name}&status=${status}`);
    }
}

export default new ScheduleService();