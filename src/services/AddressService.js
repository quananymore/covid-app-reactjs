import http from "../commons/http-common";
class AddressService{
    getProvince(){
        return http.get("/province")
    };

    getDistrict(id){
        return http.get(`/district/${id}`)
    };

    getWard(id){
        return http.get(`/ward/${id}`)
    };

    getHospital(){
        return http.get("/hospital")
    };

}

export default new AddressService();