class Utils{
    ageCalculate(born) {
        var birthDate = Date.parse(born);
        var ageDifMs = Date.now() - birthDate;
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    getStatus(status) {
        switch (status) {
            case 0:
                return "Just detect"
            case 1:
                return "Treating";
            case 2:
                return "Died";
            case 3:
                return "Cured"
            default:
                break;
        }
    }
    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
    
        return [day, month, year].join('/');
    }

}

export default new Utils();