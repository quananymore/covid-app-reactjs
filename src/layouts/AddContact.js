import { Button, Modal } from 'react-bootstrap';
import React, { useEffect, useState } from "react";
import AddressService from '../services/AddressService';
import ContactService from '../services/ContactService';
export default function AddContact({ patient }, { loadContact }) {
    const [show, setShow] = useState(false);

    const handleClose = () => {
        setShow(false);
    }
    const handleShow = () => setShow(true);
    const initContact =
    {
        address: "",
        birthDate: "",
        dateContact: "",
        gender: "",
        personName: "",
        phone: "",
        patientId: null,
        provinceId: null,
        district: null,
        wardId: null,
    };

    const initProvince = [];
    const initDistrict = [];
    const initWard = [];
    const [province, setProvince] = useState(initProvince);
    const [district, setDistrict] = useState(initDistrict);
    const [ward, setWard] = useState(initWard);
    const [contact, setContact] = useState(initContact);
    const [message, setMessage] = useState({
        result: null,
        message: ""
    });

    useEffect(() => {
        setContact({ ...contact, patientId: patient.id })
    })
    const handleChangeContact = event => {
        const { name, value } = event.target;
        setContact({ ...contact, gender: value });
        console.log(name);
        console.log(value);
        console.log(contact);
    }

    const getProvince = () => {
        AddressService.getProvince()
            .then(response => {
                setProvince(response.data);
            })
            .catch(err => {
                console.log(err);
            })
    };

    const getDistrict = id => {
        AddressService.getDistrict(id)
            .then(response => {
                setDistrict(response.data);
            })
            .catch(err => {
                console.log(err);
            })
    }

    const getWard = id => {
        AddressService.getWard(id)
            .then(response => {
                setWard(response.data);
            })
            .catch(err => {
                console.log(err);
            })
    }

    useEffect(() => {
        getProvince();
    }, []);

    const handleChangeProvince = event => {
        const { name, value } = event.target;
        console.log(name, value)
        setContact({ ...contact, [name]: value });
        getDistrict(event.target.value);
        console.log(contact)
    }
    const handleChangeDistrict = event => {
        const { name, value } = event.target;
        setContact({ ...contact, [name]: value });
        getWard(event.target.value);
    }
    const addContact = () => {
        console.log(contact);
        if (window.confirm('Add the contact?')) {
            ContactService.create(contact)
                .then(response => {
                    // console.log(response.data);
                    setMessage(response.data);
                    loadContact();
                })
                .catch(error => {
                    console.log(error);
                });
        }
    };
    return (
        <div className="submit-from">
            <div>
                <form  >
                    <div className="container">
                        <div className="row">
                            <div class="col-sm-9">
                                <div className="row">
                                    <div class="col-sm">
                                        <div className="form-group">
                                            <div className="m-5">
                                                <label>Person name</label>
                                                <input
                                                    type="text"
                                                    onChange={handleChangeContact}
                                                    name="personName"
                                                    className="form-control"
                                                    autoFocus={true}

                                                />
                                            </div>
                                            <div className="m-5">
                                                <label>Gender</label>
                                                <select
                                                    className="form-control"
                                                    name="gender"
                                                    onChange={handleChangeContact}
                                                >
                                                    <option value=""></option>
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                </select>
                                            </div>

                                            <div className="m-5">
                                                <label>contact phone</label>
                                                <input
                                                    type="text"
                                                    onChange={handleChangeContact}
                                                    name="phone"
                                                    className="form-control"
                                                />
                                            </div>

                                            <div className="m-5">
                                                <label>Birth date</label>
                                                <input
                                                    type="date"
                                                    name="birthDate"

                                                    onChange={handleChangeContact}
                                                    className="form-control"
                                                />
                                            </div>

                                            <div className="m-5">

                                                <label>Contact date</label>
                                                <input
                                                    type="date"
                                                    name="dateContact"
                                                    onChange={handleChangeContact}
                                                    className="form-control"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div className="form-group">

                                            <div className="m-5">
                                                <label>Address</label>
                                                <input
                                                    type="text"
                                                    name="address"
                                                    onChange={handleChangeContact}
                                                    className="form-control"
                                                />
                                            </div>
                                            <div className="m-5">

                                                <label>Province/City</label>
                                                <select
                                                    type="text"

                                                    onChange={handleChangeProvince}
                                                    name="provinceId"
                                                    className="form-control">
                                                    <option value="">Chọn tỉnh đi</option>
                                                    {province.map(e =>
                                                        <option value={e.code}>{e.name}</option>
                                                    )}
                                                </select>
                                            </div>
                                            <div className="m-5">

                                                <label>District</label>
                                                <select
                                                    type="text"

                                                    onChange={handleChangeDistrict}
                                                    name="district"
                                                    className="form-control">
                                                    <option value="">Chọn huyện đi</option>
                                                    {district.map(e =>
                                                        <option value={e.code}>{e.name}</option>
                                                    )}
                                                </select>
                                            </div>
                                            <div className="m-5">

                                                <label>Ward</label>
                                                <select
                                                    type="text"

                                                    onChange={handleChangeContact}
                                                    name="wardId"
                                                    className="form-control">
                                                    <option value="">Chọn huyện đi</option>
                                                    {ward.map(e =>
                                                        <option value={e.code}>{e.name}</option>
                                                    )}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="text-center">
                                        <button type="button"
                                            className="btn btn-primary mt-5"
                                            onClick={() => { addContact() }}
                                        >
                                            Add Contact
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-3">
                                <div class="text-center mt-5 p-4">
                                    <img src={patient.image} width="300" height="400" />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}