import { Button, Modal } from 'react-bootstrap';
import React, { useEffect, useState } from "react";
import AddressService from '../services/AddressService';
import ScheduleService from '../services/ScheduleService';

export default function ModalAddSchedule({ p, loadSchedule }) {
    const [show, setShow] = useState(false);
    const handleClose = () => {
        setShow(false);
    }
    const handleShow = () => setShow(true);
    const initContact =
    {
        detailLocation: "",
        fromDate: "",
        toDate: "",
        patientId: null,
        provinceId: null,
        districtId: null,
        wardId: null,
    };

    const initProvince = [];
    const initDistrict = [];
    const initWard = [];
    const [province, setProvince] = useState(initProvince);
    const [district, setDistrict] = useState(initDistrict);
    const [ward, setWard] = useState(initWard);
    const [schedule, setSchedule] = useState(initContact);
    const [message, setMessage] = useState({
        result: null,
        message: ""
    });

    useEffect(() => {
        setSchedule({ ...schedule, patientId: p.id })
    }, [])
    const handleChange = event => {
        const { name, value } = event.target;
        setSchedule({ ...schedule, [name]: value });
    }

    const getProvince = () => {
        AddressService.getProvince()
            .then(response => {
                setProvince(response.data);
            })
            .catch(err => {
                console.log(err);
            })
    };

    const getDistrict = id => {
        AddressService.getDistrict(id)
            .then(response => {
                setDistrict(response.data);
            })
            .catch(err => {
                console.log(err);
            })
    }

    const getWard = id => {
        AddressService.getWard(id)
            .then(response => {
                setWard(response.data);
            })
            .catch(err => {
                console.log(err);
            })
    }

    useEffect(() => {
        setSchedule({ ...schedule, patientId: p.id });
        getProvince();
    }, {});

    const handleChangeProvince = event => {
        const { name, value } = event.target;
        setSchedule({ ...schedule, [name]: value });
        getDistrict(event.target.value);
    }
    const handleChangeDistrict = event => {
        const { name, value } = event.target;
        setSchedule({ ...schedule, [name]: value });
        getWard(event.target.value);
    }

    const addSchedule = () => {
        console.log(schedule)
        if (window.confirm('Add the Schedule?')) {
            ScheduleService.create(schedule)
                .then(response => {
                    setMessage(response.data);
                    loadSchedule();
                })
                .catch(error => {
                    console.log(error);
                });
        }
    };

    return (
        <>
            <Button variant="primary" onClick={handleShow} >
                Add Schedule for patient
            </Button>

            <Modal show={show} onHide={handleClose}
                fullscreen={true}
            >
                <Modal.Header closeButton>
                    <Modal.Title ><h2 >Add Schedule</h2></Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="submit-from">
                        <div>
                            <form  >
                                <div className="container">
                                    <div className="row">
                                        <div class="col-sm-9">
                                            <div className="row">
                                                <div class="col-sm">
                                                    <div className="form-group">
                                                        <div className="m-5">
                                                            <label>Location</label>
                                                            <input
                                                                type="text"
                                                                onChange={handleChange}
                                                                name="detailLocation"
                                                                className="form-control"
                                                                autoFocus={true}

                                                            />
                                                        </div>
                                                        <div className="m-5">
                                                            <label>Province/City</label>
                                                            <select
                                                                type="text"

                                                                onChange={handleChangeProvince}
                                                                name="provinceId"
                                                                className="form-control">
                                                                <option value="">Chọn tỉnh đi</option>
                                                                {province.map(e =>
                                                                    <option value={e.code}>{e.name}</option>
                                                                )}
                                                            </select>
                                                        </div>
                                                        <div className="m-5">

                                                            <label>District</label>
                                                            <select
                                                                type="text"

                                                                onChange={handleChangeDistrict}
                                                                name="districtId"
                                                                className="form-control">
                                                                <option value="">Chọn huyện đi</option>
                                                                {district.map(e =>
                                                                    <option value={e.code}>{e.name}</option>
                                                                )}
                                                            </select>
                                                        </div>


                                                    </div>
                                                </div>
                                                <div class="col-sm">
                                                    <div className="form-group">
                                                        <div className="m-5">
                                                            <label>Ward</label>
                                                            <select
                                                                type="text"
                                                                onChange={handleChange}
                                                                name="wardId"
                                                                className="form-control">
                                                                <option value="">Chọn huyện đi</option>
                                                                {ward.map(e =>
                                                                    <option value={e.code}>{e.name}</option>
                                                                )}
                                                            </select>
                                                        </div>
                                                        <div className="m-5">

                                                            <label>From date</label>
                                                            <input
                                                                type="date"
                                                                name="fromDate"
                                                                onChange={handleChange}
                                                                className="form-control"
                                                            />
                                                        </div>

                                                        <div className="m-5">

                                                            <label>To date</label>
                                                            <input
                                                                type="date"
                                                                name="toDate"
                                                                onChange={handleChange}
                                                                className="form-control"
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="text-center">
                                                    <button type="button"
                                                        className="btn btn-primary mt-5"
                                                        onClick={() => { addSchedule() }}
                                                    >
                                                        Add Schedule
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-3">
                                            <div class="text-center mt-5 p-4">
                                                <img src={p.image} width="300" height="400" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </>
    );
}
