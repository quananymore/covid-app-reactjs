import { Button, Modal } from 'react-bootstrap';
import React, { useEffect, useState } from "react";
import PatientService from '../services/PatientService';
import AddressService from '../services/AddressService';
export default function ModalAddPatient({onLoadPage}) {
  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false);
  } 
  const handleShow = () => setShow(true);
  const initPatient =
    {
        address: "",
        birthDate: "",
        gender: "",
        identify: "",
        name: "",
        phoneNumber: "",
        status: null,
        provinceId: null,
        districtId: null,
        wardId: null,
        detectionDate: "",
        image: null,
        hospitalId: null,
        isDelete: 0,
    };

    const initProvince = [];
    const initDistrict = [];
    const initWard = [];
    const inithospital = [];
    // const [errors, setErrors] = useState({});
    const [province, setProvince] = useState(initProvince);
    const [district, setDistrict] = useState(initDistrict);
    const [ward, setWard] = useState(initWard);
    const [hospital, setHospital] = useState(inithospital);

    const [patient, setPatient] = useState(initPatient);
    const [image, setImage] = useState(null);
    const [message, setMessage] = useState({
        result: null,
        message: ""
    });

    const handleChange = event => {
        const { name, value } = event.target;
        setPatient({ ...patient, [name]: value });
    }

    const handleChangeImage = async (event) => { //khai báo ra hàm bất đồng bộ ()
        const file = event.target.files[0];
        const base64 = await convertBase64(file); //chặn tất cả để đợi bố async
        setImage(base64);
        const { name } = event.target;
        setPatient({ ...patient, [name]: base64 });
        // console.log(base64);
    };


    const convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);

            fileReader.onload = () => {
                resolve(fileReader.result);
            };
            fileReader.onerror = (error) => {
                reject(error);
            };
        });
    };

    const getProvince = () => {
        AddressService.getProvince()
            .then(response => {
                setProvince(response.data);
            })
            .catch(err => {
                console.log(err);
            })
    };

    const getDistrict = id => {
        AddressService.getDistrict(id)
            .then(response => {
                setDistrict(response.data);
            })
            .catch(err => {
                console.log(err);
            })
    }

    const getWard = id => {
        AddressService.getWard(id)
            .then(response => {
                setWard(response.data);
            })
            .catch(err => {
                console.log(err);
            })
    }
    const getHospital = () => {
        AddressService.getHospital()
            .then(response => {
                setHospital(response.data);
            })
            .catch(err => {
                console.log(err);
            })
    }

    useEffect(() => {
        getHospital();
    }, []);

    useEffect(() => {
        getProvince();
    }, []);

    const handleChangeProvince = event => {
        const { name, value } = event.target;
        setPatient({ ...patient, [name]: value });
        getDistrict(event.target.value);
    }
    const handleChangeDistrict = event => {
        const { name, value } = event.target;
        setPatient({ ...patient, [name]: value });
        getWard(event.target.value);
    }
    const savePatient = () => {
        if (window.confirm('Add the patient?')) {
            PatientService.create(patient)
                .then(response => {
                    // console.log(response.data);
                    setMessage(response.data);
                    onLoadPage();
                })
                .catch(error => {
                    console.log(error);
                });
        }
    };

  return (
    <>
      <Button variant="primary" onClick={handleShow} >
        Add Patient
      </Button>

      <Modal show={show} onHide={handleClose}
       fullscreen={true}
      >
        <Modal.Header closeButton>
          <Modal.Title ><h2 >Add patient</h2></Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <div className="submit-from">
            <div>
                <form  >
                    <div className="container">
                        <div className="row">
                            <div class="col-sm-9">
                                <div className="row">
                                    <div class="col-sm">
                                        <div className="form-group">
                                            <div className="m-5">
                                                <label>Patient name</label>
                                                <input
                                                    type="text"
                                                    onChange={handleChange}
                                                    name="name"
                                                    className="form-control"
                                                    autoFocus={true}

                                                />
                                                {/* {errors.isDelete && <p>{errors.isDelete}</p>} */}
                                            </div>
                                            <div className="m-5">
                                                <label>Gender</label>
                                                <select
                                                    className="form-control"
                                                    name="gender"
                                                    onChange={handleChange}
                                                >
                                                    <option value=""></option>
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                </select>
                                                {/* {errors.gender && <p>{errors.gender}</p>} */}
                                            </div>

                                            <div className="m-5">
                                                <label>Birth date</label>
                                                <input
                                                    type="date"
                                                    name="birthDate"

                                                    onChange={handleChange}
                                                    className="form-control"
                                                />
                                                {/* {errors.birthDate && <p>{errors.birthDate}</p>} */}
                                            </div>

                                            <div className="m-5">
                                                <label>Patient phone</label>
                                                <input
                                                    type="text"
                                                    name="phoneNumber"

                                                    onChange={handleChange}
                                                    className="form-control"
                                                />
                                                {/* {errors.name && <p>{errors.name}</p>} */}

                                            </div>
                                            <div className="m-5">
                                                <label>Hospital name</label>
                                                <select
                                                    className="form-control"
                                                    name="hospitalId"

                                                    onChange={handleChange}
                                                >
                                                    <option value="">Chọn bệnh viện đi</option>
                                                    {hospital.map(e =>
                                                        <option value={e.id}>{e.name}</option>
                                                    )}
                                                </select>
                                                {/* {errors.name && <p>{errors.name}</p>} */}
                                            </div>

                                            <div className="m-5">

                                                <label>Detection date</label>
                                                <input
                                                    type="date"
                                                    name="detectionDate"

                                                    onChange={handleChange}
                                                    className="form-control"
                                                />
                                                {/* {errors.name && <p>{errors.name}</p>} */}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div className="form-group">
                                            <div className="m-5">
                                                <label>Identify</label>
                                                <input
                                                    type="text"
                                                    name="identify"

                                                    onChange={handleChange}
                                                    className="form-control"
                                                />
                                                {/* {errors.name && <p>{errors.name}</p>} */}
                                            </div>
                                            <div className="m-5">
                                                <label>Address</label>
                                                <input
                                                    type="text"

                                                    name="address"
                                                    onChange={handleChange}
                                                    className="form-control"
                                                />
                                                {/* {errors.name && <p>{errors.name}</p>} */}
                                            </div>
                                            <div className="m-5">

                                                <label>Province/City</label>
                                                <select
                                                    type="text"

                                                    onChange={handleChangeProvince}
                                                    name="provinceId"
                                                    className="form-control">
                                                    <option value="">Chọn tỉnh đi</option>
                                                    {province.map(e =>
                                                        <option value={e.code}>{e.name}</option>
                                                    )}
                                                </select>
                                                {/* {errors.name && <p>{errors.name}</p>} */}
                                            </div>
                                            <div className="m-5">

                                                <label>District</label>
                                                <select
                                                    type="text"

                                                    onChange={handleChangeDistrict}
                                                    name="districtId"
                                                    className="form-control">
                                                    <option value="-1">Chọn huyện đi</option>
                                                    {district.map(e =>
                                                        <option value={e.code}>{e.name}</option>
                                                    )}
                                                </select>
                                                {/* {errors.name && <p>{errors.name}</p>} */}
                                            </div>
                                            <div className="m-5">

                                                <label>Ward</label>
                                                <select
                                                    type="text"

                                                    onChange={handleChange}
                                                    name="wardId"
                                                    className="form-control">
                                                    <option value="-1">Chọn huyện đi</option>
                                                    {ward.map(e =>
                                                        <option value={e.code}>{e.name}</option>
                                                    )}
                                                </select>
                                                {/* {errors.name && <p>{errors.name}</p>} */}
                                            </div>
                                            <div className="m-5">

                                                <label>Status</label>
                                                <select
                                                    className="form-control"

                                                    onChange={handleChange}
                                                    name="status"
                                                >
                                                    <option value=""></option>
                                                    <option value="0">Just detection</option>
                                                    <option value="1">Be treating</option>
                                                    <option value="2">Died</option>
                                                    <option value="3">Cured</option>

                                                </select>
                                                {/* {errors.name && <p>{errors.name}</p>} */}
                                            </div>
                                        </div>
                                    </div>


                                    <div className="text-center">
                                        <button type="button"
                                            className="btn btn-primary mt-5"
                                            onClick={() => { savePatient() }}
                                        >
                                            Add Patient
                                        </button>
                                    </div>
                                    <div className="text-center">
                                        {message.result == 1 ? (<p class="text-primary">{message.message}</p>) : (<p class="text-danger">{message.message}</p>)}
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-3">
                                <div class="text-center mt-5 p-4">
                                    {/* <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" /> */}
                                    <img src={image}  width="300" height="400"/>
                                    <input type="file"
                                        class="text-center center-block file-upload"
                                        name="image"
                                        onChange={handleChangeImage}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        </Modal.Body>
      </Modal>
    </>
  );
}
