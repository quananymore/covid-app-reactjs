import React, { Component } from 'react'
import PatientService from '../services/PatientService';
import { Link } from "react-router-dom";
import Button from 'react-bootstrap/Button';
import ModalAddPatient from '../layouts/ModalAddPatient';
import Utils from '../commons/Utils'
export default class PatientList extends Component {
    constructor({props}) {
        super(props);
        this.getPatients = this.getPatients.bind(this);
        this.refreshList = this.refreshList.bind(this);
        this.search = this.search.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChange1 = this.handleChange1.bind(this);
        this.state = {
            patients: [
            ],
            currentPatient: null,
            searchName: "",
            searchStatus: ""
        };
    }

    handleChange1(e) {
        var status = e.target.value;
        this.setState({
            ...this.state,
            searchStatus: status
        });
    }

    handleChange(e) {
        var status = e.target.value;
        this.setState({
            ...this.state,
            searchName: status
        });
    }

    componentDidMount() {
        this.getPatients();
        // 
    }

    getPatients() {
        console.log("Get list Patient function")
        PatientService.getAll()
            .then(resp => {
                this.setState({
                    patients: resp.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    refreshList() {
        this.getPatients();
        this.setState({
            currentPatient: null,
            currentIndex: -1
        });
    }

    search() {
        this.setState({
            currentIndex: -1,
            currentPatient: null
        });
        var name = this.state.searchName;
        var status = this.state.searchStatus;

        console.log(name);
        console.log(status);
        PatientService.search(name, status)
            .then(response => {
                this.setState({
                    patients: response.data
                });
            })
            .catch(e => {
                console.log(e);
            });

    }

    delete(id) {
        console.log(id);
        PatientService.delete(id)
            .then(response => {
                this.search();
            })
            .catch(e => {
                console.log(e);
            });
    }

    

    render() {
        const { patients } = this.state;

        return (
            <div className="container">
                <div className="list row">
                    <div className="col-md-12">
                        <div className="form-control">
                            <label>Name</label>
                            <input
                                type="text"
                                className="form-control"
                                value={this.searchName}
                                placeholder="Search by name"
                                onChange={this.handleChange}
                            />
                            <label>Status</label>
                            <select
                                className="form-control"
                                name="status"
                                onChange={this.handleChange1}
                            >
                                <option value=""></option>
                                <option value="0">Just detection</option>
                                <option value="1">Be treating</option>
                                <option value="2">Died</option>
                                <option value="3">Cured</option>

                            </select>


                        </div>
                        <div className="d-flex justify-content-center">
                            <div className="m-5">
                                <Button
                                    onClick={this.search}
                                >
                                    Search
                                </Button>
                            </div>
                            <div className="m-5">
                                <ModalAddPatient onLoadPage = {this.getPatients}/>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-12">
                        <h2>List Patients</h2>
                        <table className="table table-bordered" >
                            <thead>
                                <tr>
                                    <th>Patient Name</th>
                                    <th>Hospital Name</th>
                                    <th>Gender</th>
                                    <th>Age</th>
                                    <th>Current status</th>
                                    <th>Detection Time</th>
                                    <th className="text-center">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                {patients &&
                                    patients.map((patient) => (
                                        <tr key={patient.id}>
                                            <td>{patient.name}</td>
                                            <td>{patient.hospitalName}</td>
                                            <td>{patient.gender}</td>
                                            <td>{Utils.ageCalculate(patient.birthDate)}</td>
                                            <td>{Utils.getStatus(patient.status)}</td>
                                            <td>{Utils.formatDate(patient.detectionDate)}</td>
                                            <td className="text-center">
                                                <button type="button" className="btn btn-warning">
                                                    <Link
                                                        to={"/patient/" + patient.id}

                                                    >
                                                        Edit
                                                    </Link>
                                                </button>

                                                <button type="button" 
                                                className="btn btn-danger m-2"
                                                onClick={() =>{if(window.confirm('Delete the item?')){this.delete(patient.id)};} }
                                                >
                                                    Delete
                                                </button>
                                            </td>
                                        </tr>
                                    )
                                    )}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}
