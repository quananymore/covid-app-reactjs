import React from 'react';
import PatientService from '../services/PatientService';
import "../commons/Utils";
import ScheduleService from '../services/ScheduleService';
class Patient extends React.Component {
    constructor(props) {
        super(props);
        console.log(props);
        this.onChangeStatus = this.onChangeStatus.bind(this);
        this.getPatient = this.getPatient.bind(this);
        this.getSchedule = this.getSchedule.bind(this);
        // this.getContact = this.getContact.bind(this);

        this.updatePatient = this.updatePatient.bind(this);

        this.state = {
            currentPatient: {
                patient: {
                    patient: {
                        id: null,
                        address: "",
                        birthDate: "",
                        gender: "",
                        identify: "",
                        name: "",
                        phoneNumber: "",
                        status: null,
                        provinceId: null,
                        districtId: null,
                        wardId: null,
                        detectionDate: "",
                        image: "",
                        hospitalId: null,
                        isDelete: 0,
                    },
                    hospitalName: "",
                    provinceName: "",
                    wardName: "",
                    districtName: ""
                },
                schedules:
                    [{
                        schedule: {
                            id: null,
                            provinceId: null,
                            districtId: null,
                            wardId: null,
                            detailLocation: "",
                            fromDate: "",
                            toDate: "",
                            patientId: null,
                        },
                        province: "",
                        district: "",
                        ward: ""
                    }
                    ],
                contacts: [
                    {
                        contact: {
                            id: null,
                            patientId: null,
                            dateContact: "",
                            personId: null,
                            address: "",
                            provinceId: null,
                            district: null,
                            wardId: null
                        },
                        person: {
                            id: null,
                            identify: "",
                            phoneNumber: "",
                            name: "",
                            gender: "",
                            birthDate: "",
                            address: "",
                            provinceId: null,
                            districtId: null,
                            wardId: null,
                            status: null
                        },
                        province: "",
                        ward: "",
                        district: "",
                    }
                ]
                ,

            },
            hospital: {
                hospital: {
                    id: null,
                    name: "",
                    address: "",
                    phone: "",
                    districtId: null,
                    provinceId: null,
                    wardId: null
                },
                province: "",
                ward: "",
                district: "",
            },
            message: ""
        };
    }

    componentDidMount() {
        console.log('didMount=> id:' + this.props.match.params.id)
        this.getPatient(this.props.match.params.id);
        this.getSchedule(this.props.match.params.id);

    }

    onChangeStatus(e) {
        const status = e.target.value;
        this.setState(function (prevState) {
            return {
                currentPatient: {
                    ...prevState.currentPatient,
                    status: status
                }
            }
        })
    }

    getPatient(id) {
        console.log("getPatient");
        PatientService.get(id)
            .then(response => {
                this.setState({
                    currentPatient: response.data
                });
                // console.log(this.state.currentPatient)
                // var schedules = this.state.currentPatient.schedules
                // schedules.map(schedule=>{
                //     console.log(schedule.schedule)
                // })

            })
            .catch(e => {
                console.log("🚀 ~ file: PatientComponent.js ~ line 66 ~ PatientComponent ~ getPatient ~ e", e)
            });
    }

    getSchedule(id) {
        ScheduleService.getSchedule(id)
            .then(response => {
                this.setState({
                    ...this.state,
                    schedules: response.data
                });
                console.log(this.state.schedules)
            })
            .catch(e => {
                console.log("🚀 ~ file: PatientComponent.js ~ line 66 ~ PatientComponent ~ getPatient ~ e", e)
            });
    }

    updatePatient() {
        PatientService.update(
            this.state.currentPatient
        )
            .then(response => {
                console.log(response.data);
                this.setState({
                    message: "The patient was updated successfully!"
                });
            })
    }
    render() {

        const { currentPatient } = this.state;
        // const { schedule } = this.state.currentPatient.schedules;
        return (
            <div>
                {currentPatient ? (
                    <div className="edit-form">
                        <h2 className="text-center">Detail Patient</h2>
                        <form>
                            <div className="container">
                                <div className="row">
                                    <div class="col-sm">
                                        <div className="form-group">
                                            <label>Patient name</label>
                                            <input
                                                type="text"
                                                readOnly={true}
                                                className="form-control"
                                                value={currentPatient.patient.patient.name}
                                            />
                                            <label>Gender</label>
                                            <input
                                                type="text"
                                                readOnly={true}
                                                className="form-control"
                                                value={currentPatient.patient.patient.gender}
                                            />
                                            <label>Age</label>
                                            <input
                                                type="text"
                                                readOnly={true}
                                                className="form-control"
                                                value={ageCalculate(currentPatient.patient.patient.birthDate)}
                                            />
                                            <label>Patient phone</label>
                                            <input
                                                type="text"
                                                readOnly={true}
                                                className="form-control"
                                                value={currentPatient.patient.patient.phoneNumber}
                                            />
                                            <label>Hospital name</label>
                                            <input
                                                type="text"
                                                readOnly={true}
                                                className="form-control"
                                                value={currentPatient.patient.hospitalName}
                                            />
                                            <label>Detection date</label>
                                            <input
                                                type="text"
                                                readOnly={true}
                                                className="form-control"
                                                value={formatDate(currentPatient.patient.patient.detectionDate)}
                                            />
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div className="form-group">
                                            <label>Identify</label>
                                            <input
                                                type="text"
                                                readOnly={true}
                                                className="form-control"
                                                value={currentPatient.patient.patient.identify}
                                            />
                                            <label>Address</label>
                                            <input
                                                type="text"
                                                readOnly={true}
                                                className="form-control"
                                                value={currentPatient.patient.patient.address}
                                            />
                                            <label>Ward</label>
                                            <input
                                                type="text"
                                                readOnly={true}
                                                className="form-control"
                                                value={currentPatient.patient.wardName}
                                            />
                                            <label>District</label>
                                            <input
                                                type="text"
                                                readOnly={true}
                                                className="form-control"
                                                value={currentPatient.patient.districtName}
                                            />
                                            <label>Province/City</label>
                                            <input
                                                type="text"
                                                readOnly={true}
                                                className="form-control"
                                                value={currentPatient.patient.provinceName}
                                            />
                                            <label>Status</label>
                                            <input
                                                type="text"
                                                readOnly={true}
                                                className="form-control"
                                                value={getStatus(currentPatient.patient.patient.status)}
                                            />
                                        </div>
                                    </div>
                                    <div class="col align-self-end">
                                        <div class="profile-img">
                                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS52y5aInsxSm31CvHOFHWujqUx_wWTS9iM6s7BAm21oEN_RiGoog" alt="" />
                                        </div>
                                        <h5>Update current status: </h5>
                                        <label>Status</label>
                                        <select
                                            className="form-control"
                                            name="status"
                                            onChange={this.handleChange1}
                                        >
                                            <option value=""></option>
                                            <option value="0">Just detection</option>
                                            <option value="1">Be treating</option>
                                            <option value="2">Died</option>
                                            <option value="3">Cured</option>

                                        </select>

                                        <button type="button"
                                            className="btn btn-primary mt-2"
                                            onClick={() => { if (window.confirm('Update the patient?')) { }; }}
                                        >
                                            Update
                                        </button>

                                    </div>
                                </div>
                            </div>

                        </form>

                       
                    </div>
                ) : (
                    <div>
                        <br />
                        <p>Please click on a Patient...</p>
                    </div>
                )}

            </div>
        );
    }
}
function ageCalculate(born) {
    var birthDate = Date.parse(born);
    var ageDifMs = Date.now() - birthDate;
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

function getStatus(status) {
    switch (status) {
        case 0:
            return "Just detect"
        case 1:
            return "Treating";
        case 2:
            return "Died";
        case 3:
            return "Cured"
        default:
            break;
    }
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [day, month, year].join('/');
}

export default Patient;