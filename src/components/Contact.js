import React from 'react'

export default function Contact({contact}) {
    return (
        <tr>
            {/* <td>Một nơi nào đó</td> */}
            <td>{contact.dateContact}</td>
            <td>{contact.personName}</td>
            <td>{contact.gender}</td>
            <td>{contact.phone}</td>
            <td>{contact.birthDate}</td>
            <td>{contact.address}</td>
            <td>{contact.ward}</td>
            <td>{contact.district}</td>
            <td>{contact.province}</td>
    </tr>
    )
}
