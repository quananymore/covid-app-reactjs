import React from 'react'

export default function Schedule({ schedule }) {
    return (
        <tr>
            <td>{schedule.detailLocation}</td>
            <td>{schedule.ward}</td>
            <td>{schedule.district}</td>
            <td>{schedule.province}</td>
            <td>{schedule.fromDate}</td>
            <td>{schedule.toDate}</td>
        </tr>
    )
}
