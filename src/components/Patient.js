import React, { useEffect, useState } from 'react'
import PatientService from '../services/PatientService';
import ScheduleService from '../services/ScheduleService';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Schedule from './Schedule';
import Contact from './Contact';
import ContactService from '../services/ContactService';
import ModalAddContact from '../layouts/ModalAddContact'
import ModalAddSchedule from '../layouts/ModuleAddSchedule';
import Utils from '../commons/Utils';

const Patient2 = props => {

    const initPatient = {
        patient: {
            id: props.match.params.id,
            address: "",
            birthDate: "",
            gender: "",
            identify: "",
            name: "",
            phoneNumber: "",
            status: null,
            detectionDate: "",
            image: "",
            province: "",
            district: "",
            ward: "",
        },
        hospital: {
            id: null,
            name: "",
            address: "",
            phone: "",
            province: "",
            district: "",
            ward: "",
        },
        contacts:[
            {
                id: null,
                patientId: null,
                dateContact: "",
                birthDate: "",
                personName: "",
                gender: "",
                phone: "",
                address: "",
                province: "",
                ward: "",
                district: "",
            }
        ],
        schedules:[
            {
                id: null,
                detailLocation: "",
                fromDate: "",
                toDate: "",
                patientId: null,
                province: "",
                district: "",
                ward: "",
            }
        ]
    };
    
    const initMessage = {
        result: null,
        message: ""
    }
    const [patient, setPatient] = useState(initPatient);
    const [message, setMessage] = useState(initMessage);
    const updateStatus = (e) => {
        PatientService.update(e)
            .then(response => {
                setMessage(response.data);
                getPatient(patient.patient.id);
                // if (message.result == 1) {
                //     console.log('hi');
                // }
            })
            .catch(err => {
                console.log(err)
            })
    }
    //Lay thông tin bệnh nhân
    const getPatient = id => {
        PatientService.get(id)
            .then(response => {
                setPatient(response.data);
            })
            .catch(e => {
                console.log("🚀 ~ file: PatientComponent.js ~ line 66 ~ PatientComponent ~ getPatient ~ e", e)
            });
    }
    const onChangeStatus = (e) => {
        patient.patient.status = e.target.value
    }

    // Ham useEffect Hook
    useEffect(() => {
        getPatient(props.match.params.id);
    }, [props.match.params.id]);

    useEffect(() => {
        const timer = setInterval(() => {
          PatientService.exportExcel()
          .then(
              console.log("done")
          )
        }, 1800000);
        return () => clearTimeout(timer);
      }, []);

    return (
        <div>
            {patient ? (
                <div className="edit-form">
                    <h2 className="text-center">Detail Patient</h2>
                    <form>
                        <div className="container">
                            <div className="row">
                                <div class="col-sm">
                                    <div className="form-group">
                                        <label>Patient name</label>
                                        <input
                                            type="text"
                                            readOnly={true}
                                            className="form-control"
                                            value={patient.patient.name}
                                        />
                                        <label>Gender</label>
                                        <input
                                            type="text"
                                            readOnly={true}
                                            className="form-control"
                                            value={patient.patient.gender}
                                        />
                                        <label>Age</label>
                                        <input
                                            type="text"
                                            readOnly={true}
                                            className="form-control"
                                            value={Utils.ageCalculate(patient.patient.birthDate)}
                                        />
                                        <label>Patient phone</label>
                                        <input
                                            type="text"
                                            readOnly={true}
                                            className="form-control"
                                            value={patient.patient.phoneNumber}
                                        />
                                        <label>Hospital name</label>
                                        <input
                                            type="text"
                                            readOnly={true}
                                            className="form-control"
                                            value={patient.hospital.name}
                                        />
                                        <label>Detection date</label>
                                        <input
                                            type="text"
                                            readOnly={true}
                                            className="form-control"
                                            value={Utils.formatDate(patient.patient.detectionDate)}
                                        />
                                    </div>
                                </div>
                                <div class="col-sm">
                                    <div className="form-group">
                                        <label>Identify</label>
                                        <input
                                            type="text"
                                            readOnly={true}
                                            className="form-control"
                                            value={patient.patient.identify}
                                        />
                                        <label>Address</label>
                                        <input
                                            type="text"
                                            readOnly={true}
                                            className="form-control"
                                            value={patient.patient.address}
                                        />
                                        <label>Ward</label>
                                        <input
                                            type="text"
                                            readOnly={true}
                                            className="form-control"
                                            value={patient.patient.ward}
                                        />
                                        <label>District</label>
                                        <input
                                            type="text"
                                            readOnly={true}
                                            className="form-control"
                                            value={patient.patient.district}
                                        />
                                        <label>Province/City</label>
                                        <input
                                            type="text"
                                            readOnly={true}
                                            className="form-control"
                                            value={patient.patient.province}
                                        />
                                        <label>Status</label>
                                        <input
                                            type="text"
                                            readOnly={true}
                                            className="form-control"
                                            value={Utils.getStatus(patient.patient.status)}
                                        />
                                    </div>
                                </div>

                                <div class="col align-self-end">
                                    <div class="profile-img">
                                        <img src={patient.patient.image} width="300" height="400" />
                                    </div>
                                    <h5>Update current status: </h5>
                                    <label>Status</label>
                                    <select
                                        className="form-select"
                                        name="status"
                                        onChange={onChangeStatus}
                                    >
                                        <option value=""></option>
                                        <option value="0">Just detection</option>
                                        <option value="1">Be treating</option>
                                        <option value="2">Died</option>
                                        <option value="3">Cured</option>

                                    </select>
                                    <button type="button"
                                        className="btn btn-primary mt-2"
                                        onClick={() => { if (window.confirm('Update the patient?')) { updateStatus(patient.patient) }; }}
                                    >
                                        Update
                                    </button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            ) : (
                <div>
                    <br />
                    <p>Please click on a Patient...</p>
                </div>
            )}


            <div className="container mt-4">
                <div className="row">
                    <div className="col-8">
                        <div className="mb-5">
                            <h4>Schedule List </h4>

                            {patient.schedules.length > 0 ? (
                                <div class="table-wrapper-scroll-y my-custom-scrollbar">

                                    <table class="table table-bordered table-striped mb-0">
                                        <thead>
                                            <tr>
                                                <th scope="col">Location</th>
                                                <th scope="col">Ward</th>
                                                <th scope="col">District</th>
                                                <th scope="col">Province</th>
                                                <th scope="col">From date</th>
                                                <th scope="col">To date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {patient.schedules.map(e => {
                                                return <Schedule schedule={e} />

                                            })}
                                        </tbody>
                                    </table>

                                </div>
                            ) : (
                                <div>
                                    <br />
                                    <p>No schedule to show - this one is developer and he have been working from home</p>
                                </div>
                            )}
                        </div>
                        <div className="mt-4">
                            <h4>Contact List </h4>
                            {patient.contacts.length > 0 ? (

                                <div class="table-wrapper-scroll-y my-custom-scrollbar mb-5">

                                    <table class="table table-bordered table-striped mb-0">
                                        <thead>
                                            <tr>
                                                <th scope="col">Date contact</th>
                                                <th scope="col">Person name</th>
                                                <th scope="col">Gender</th>
                                                <th scope="col">Phone</th>
                                                <th scope="col">Age</th>
                                                <th scope="col">Address</th>
                                                <th scope="col">Ward</th>
                                                <th scope="col">District</th>
                                                <th scope="col">Province</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {patient.contacts.map(e => {
                                                return <Contact contact={e} />
                                            })}
                                        </tbody>
                                    </table>

                                </div>

                            ) : (
                                <div>
                                    <br />
                                    <p>No Contact to show - this one is developer and he have been working from home</p>
                                </div>
                            )}
                        </div>
                    </div>

                    <div className="col-4">
                        <div className="text-center">
                            <ModalAddSchedule p={patient.patient} loadSchedule={() => getPatient(props.match.params.id)} />
                        </div>

                        <div className="text-center m-5">
                            <ModalAddContact patient={patient.patient} loadContact={() => getPatient(props.match.params.id)} />
                        </div>
                        <div className="text-center">
                        {/* /export/${props.match.params.id} */}
                        
                      
                           
                                <a className="btn btn-primary cursor-pointer text-white" href={`http://localhost:8080/export/${props.match.params.id}`}>Export<FontAwesomeIcon icon="coffee" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Patient2;


